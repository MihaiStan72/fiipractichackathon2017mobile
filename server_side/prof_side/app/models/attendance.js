var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var AttendanceSchema   = new Schema({
    user: String,
    day:  { type: Date, default: Date.now }
});
module.exports = mongoose.model('Attendance', AttendanceSchema);