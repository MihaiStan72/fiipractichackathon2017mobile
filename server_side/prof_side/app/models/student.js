var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var StudentSchema   = new Schema({
    user: String,
    uid: String
});

module.exports = mongoose.model('Student', StudentSchema);