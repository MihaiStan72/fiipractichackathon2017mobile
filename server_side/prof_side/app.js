var express = require('express')
var app = express()
var bodyParser = require('body-parser');

var bodyParser = require('body-parser');

var Course     = require('./app/models/course');
var Student    = require('./app/models/student');
var Attendance = require('./app/models/attendance')

var mongoose   = require('mongoose');
mongoose.connect('mongodb://localhost/prof_side');; // connect to our database


// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var router = express.Router();   
var request = require('request');
request('http://localhost:3000/api/student', function (error, response, body) {
  if (!error) {
      //console.log(body);
      var array = JSON.parse(body);
      console.log(array[1].user);
      Student.remove({}, function(err, student){
           if (err)
                console.log(err);

            console.log( 'Successfully deleted' );
            for(let i in array){
              //console.log(array[i]);
              var student = new Student;
              student.user = array[i].user;
              student.uid = array[i].uid;
              console.log(student);
              student.save(function(err) {
              if (err)
                console.log(err);
            console.log({ message: 'Student created!' });
        });
        }
      });
  }
  else{
      console.log(error);
  }
})
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

router.route('/student')
    .get(function(req, res) {
        Student.find(function(err, student) {
            if (err)
                res.send(err);

            res.json(student);
        });
    });

router.route('/attendance')
    .post(function(req, res) {
        
         
        console.log(req.body);   
        let uid = req.body.uid; //req.body.deviceId;

        // save the check and check for errors
        Student.findOne({uid: uid}).exec( function(err, student) {
            if (err)
                res.send(err);
            var attendance = new Attendance();
            attendance.user = student.user; 
            console.log(attendance);
            attendance.save(function(err) {
            if (err)
                {
                    console.log("llala");
                    res.send(err);
                }

            res.json({ message: 'Attendance created!' });
        })});
   
    })
    .get(function(req, res) {
        Attendance.find(function(err, attendance) {
            if (err)
                res.send(err);

            res.json(attendance);
        });
    })
     .delete(function(req, res) {
        Attendance.remove({
            _id: req.body.attendance_id
        }, function(err, attendance) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });
router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });   
});

app.use('/api', router);


app.listen(3001, '0.0.0.0', function () {
  console.log('Example app listening on port');
} )