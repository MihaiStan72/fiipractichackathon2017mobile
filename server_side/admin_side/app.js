var express = require('express')
var app = express()
var bodyParser = require('body-parser');

var bodyParser = require('body-parser');
var _ = require('lodash');
var Student     = require('./app/models/student');

var mongoose   = require('mongoose');
mongoose.connect('mongodb://localhost/admin_side'); // connect to our database


// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var router = express.Router();   

router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});
router.route('/student')
    .post(function(req, res) {
        
        var student = new Student();   
        console.log(req.body);   
        student.user = req.body.user;  
        student.uid = req.body.deviceId||'';

        // save the check and check for errors
        student.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'Student created!' });
        });
   
    })
    .get(function(req, res) {
        Student.find(function(err, student) {
            if (err)
                res.send(err);

            res.json(student);
        });
    })
     .delete(function(req, res) {
        Student.remove({
            _id: req.body.student_id
        }, function(err, student) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });

    router.route('/student/:user')

    .get(function(req, res) {
        console.log(req.params);
        Student.findOne({user: req.params.user}).exec( function(err, student) {
            if (err)
                res.send(err);
            res.json(student);
        });
    })
    .post(function(req, res) {
        console.log(req.params);
        let pa=req.params.user;
        pa=_.deburr(pa);//TODO
        pa=_.trimEnd(pa, 'a');

        console.log(pa);
        console.log(req.body);
        Student.findOne({user: pa}).exec( function(err, student) {
            if(student===null){
                console.log("errorrrrrrr");
                res.send({message:2});//no student
            }else{
                if (err)
                    res.send({err});
                if(student.uid == ''||student.uid==req.body.uid){
                    student.uid = req.body.uid;  // update the student info
                    // save
                    student.save(function(err) {
                        if (err)
                            res.send(err);

                        res.json({ message: 0 });//all good
                    });
                }
                else{
                    res.json({ message: 1 });//uid existent
                }
            }

        });
    });

    

router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });   
});

app.use('/api', router);


app.listen(3000, '0.0.0.0', function () {
  console.log('Example app listening on port');
} )