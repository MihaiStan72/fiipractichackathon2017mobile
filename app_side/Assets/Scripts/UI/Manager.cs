﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

namespace UI {

    public enum LoginRequestAnswerType {
        InvalidUser,
        AlreadyRegistered,
        OK
    }

    public class Manager : MonoBehaviour {

        public GameObject Label;
        public GameObject loginPanel;
        public GameObject seminarPanel;
        public GameObject invalidUserError;
        public GameObject alreadyRegisterdError;
      
        public LoginRequestAnswerType type;
        [SerializeField]
        string user;
        string password;

        string baseURL = "http://192.168.18.1:3000";

        public void ProcessLogin() {
            Label.SetActive(true);
            StartCoroutine(IsValidText());
        }

        public void SendText(string type, string text) {
            if(type == "user") {
                user = text;
            } else if (type == "password") {
                password = text;
            }
        }

        IEnumerator IsValidText() {
            WWWForm loginForm = new WWWForm();
            Debug.Log(SystemInfo.deviceUniqueIdentifier);
            loginForm.AddField("uid", SystemInfo.deviceUniqueIdentifier);
            user = user.Replace(" ", string.Empty);
            Debug.Log(user);
            string targetURL = baseURL + "/api/student/" + user;
            //targetURL = "www.httpbin.org/post";
            WWW request = new WWW(targetURL, loginForm);
            yield return request;
            Label.SetActive(false);
            Debug.Log(request.text);
            string jsonString = request.text;
            //jsonString = Utility.JsonHelper.FixJson(request.text);
            Models.Response response = Utility.JsonHelper.DeserializeJSON<Models.Response>(jsonString);
            LoginRequestAnswerType type = LoginRequestAnswerType.OK;
            if (response.message == 1) {
                type = LoginRequestAnswerType.AlreadyRegistered;
            } else if (response.message == 2) {
                type = LoginRequestAnswerType.InvalidUser;
            }
            TreatLoginResponse(type);
            //PresentSeminar();
        }

        void TreatLoginResponse(LoginRequestAnswerType type) {
            if (type == LoginRequestAnswerType.InvalidUser) {
                PresentInvalidUser();
                Invoke("HideInvalidUserError", 3);
            } else if (type == LoginRequestAnswerType.AlreadyRegistered) {
                PresentAlreadyRegisteredError();
                Invoke("HideAlreadyRegisteredError", 3);
            } else if (type == LoginRequestAnswerType.OK) {
                PresentSeminar();
                PlayerPrefs.SetInt(Constants.isLoggedInKey, 1);
            }
        }

        void PresentSeminar() {
            loginPanel.SetActive(false);
            seminarPanel.SetActive(true);
        }

        void PresentAlreadyRegisteredError() {
            //loginPanel.SetActive(false);
            alreadyRegisterdError.SetActive(true);
        }

        void PresentInvalidUser() {
            //loginPanel.SetActive(false);
            invalidUserError.SetActive(true);
        }

        void HideInvalidUserError () {
            invalidUserError.SetActive(false);
        }

        void HideAlreadyRegisteredError () {
            alreadyRegisterdError.SetActive(false);
        }

        void Deactivate(LoginRequestAnswerType type) {
            if (type == LoginRequestAnswerType.InvalidUser) {
                invalidUserError.SetActive(false);
            } else {
                alreadyRegisterdError.SetActive(false);
            }
        }
    }

}
