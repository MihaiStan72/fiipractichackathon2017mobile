﻿using UnityEngine;
using System.Collections;

namespace UI {

    public class TextFieldHandler : MonoBehaviour {
        public UI.Manager manager;
        public TMPro.TextMeshProUGUI textField;
        public string type;

        public void SendText () {
            manager.SendText(type, textField.text);
        }

    }
}
