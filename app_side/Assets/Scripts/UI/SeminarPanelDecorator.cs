﻿using UnityEngine;
using System.Collections;

public class SeminarPanelDecorator : MonoBehaviour {

    TMPro.TextMeshProUGUI textLabel;

    void DecorateWithModel(Models.Seminar seminar) {
        textLabel.text = "Materie: " + seminar.materie + "\nProfesor: " + seminar.profesor + "\nGrupa: " + seminar.grupa;
    }
}
