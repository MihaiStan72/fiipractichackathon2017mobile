﻿using UnityEngine;
using System.Collections;

namespace Models {
    public class Seminar {
        public string materie;
        public string profesor;
        public string grupa;
    }

    public class Response {
        public int message;
    }
}
