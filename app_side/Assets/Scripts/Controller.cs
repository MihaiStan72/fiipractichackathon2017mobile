﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {

    public GameObject LoginPanel;
    public GameObject SeminarPanel;

    public bool debugDelete;

	// Use this for initialization
	void Start () {
        if (!debugDelete)
        {
            int loggedOn = PlayerPrefs.GetInt(Constants.isLoggedInKey, 0);
            if (loggedOn != 0)
            {
                SeminarPanel.SetActive(true);
            }
            else
            {
                LoginPanel.SetActive(true);
            }
        } else {
            PlayerPrefs.DeleteAll();
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
