﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Utility {

    public class JsonHelper {

        public static type DeserializeJSON <type> (string JSONString) {
            return JsonUtility.FromJson<type>(JSONString);
        } 

        static string SerializeJSON <type> (type objectToSerialize) {
            return JsonUtility.ToJson(objectToSerialize, true);
        }

        public static T[] FromJson<T>(string json)
        {
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
            return wrapper.Items;
        }

        public static string ToJson<T>(T[] array)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper);
        }

        public static string ToJson<T>(T[] array, bool prettyPrint)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper, prettyPrint);
        }

        public static string FixJson(string value)
        {
            value = "{\"Items\":" + value + "}";
            return value;
        }

        [Serializable]
        private class Wrapper<T>
        {
            public T[] Items;
        }
    }

}