﻿using UnityEngine;
using System.Collections;

public class Seminar : MonoBehaviour {
    public TMPro.TextMeshProUGUI statusLabel;
    public GameObject loadingPanel;

    float requestStartTime;
    [SerializeField]
    float timeoutAfter;
    WWW request;

    public enum AttendanceResponse {
        AllGood = 0,
        Bad = 1
    }

    [SerializeField]
    string adress;

	// Use this for initialization
	void Start () {
        requestStartTime = Time.time;
        loadingPanel.SetActive(true);
        statusLabel.gameObject.SetActive(false);
        StartCoroutine(AttendanceRequest());
        StartCoroutine(HasRequestTimedOut());
	}
	
    IEnumerator HasRequestTimedOut() {
        yield return new WaitForSeconds(timeoutAfter);
        request = null;
        StopCoroutine(AttendanceRequest());
        Models.Response failure = new Models.Response();
        failure.message = 1;//bad
        TreatResponse(failure);
    }

    IEnumerator AttendanceRequest() {
        WWWForm form = new WWWForm();
        form.AddField("uid", SystemInfo.deviceUniqueIdentifier);
        request = new WWW(adress, form);
        yield return request;
        if (request.text != "")
        {
            string jsonText = request.text;
            Models.Response response = Utility.JsonHelper.DeserializeJSON<Models.Response>(jsonText);
            TreatResponse(response);
        }
    }

    void TreatResponse(Models.Response response) {
        loadingPanel.SetActive(false);
        statusLabel.gameObject.SetActive(true);
        StopAllCoroutines();
        if (response.message == (int)AttendanceResponse.AllGood) {
            statusLabel.text = "Status: " + "prezent.";
            statusLabel.color = new Color(0, 249, 0);
        } else {
            statusLabel.text = "Error. Restart app or contact teacher.";
            statusLabel.color = new Color(255, 0, 0);
        }
    }

	// Update is called once per frame
	void Update () {
	
	}
}
